package com.zanaco.azuretest.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/azure")
public class MainCnt {

    @GetMapping("/hello")
    public String hello() {

        return "hello world!";
    }
}
